import java.util.Random;
import java.util.Vector;

public class Barbershop {

	private int c, b, cut;
	private int mutexS;
	private int numberOfChairs, waiting;
	public static int chairNumber = 1;
	private Vector listOfCustomers;
	

	public Barbershop(int numberOfChairs) {
		this.numberOfChairs = numberOfChairs;
		c = 0;
		b = 0;
		cut = 0;
		mutexS = 1;

		listOfCustomers = new Vector<Customer>();

	}
	
	public synchronized void process(){
		
	}

	public void wantToCut() {
		if (waiting <= 0) {
			System.out.println("The barber is sleeping now");
		}
		customerDecrement(); 
		mutexDown();
		waiting--; 
		barberIncrement(); 
		mutexUp();
		System.out.println("Barber: cutting hair");
		cuttingDecrement();
	}

	public void customerArrived(Customer c) {
		if (waiting < numberOfChairs) {
			listOfCustomers.add(c);
			waiting++;
			System.out.println("Customer " + c.getID()
					+ ": arrived, sitting in the waiting room");
			customerIncrement();
		} else if (waiting >= numberOfChairs) {
			System.out
					.println("Customer "
							+ c.getID()
							+ " cannot cut his hair here; the barber shop is full, customer should go to anotehr barbershop");
		}

		if (!listOfCustomers.isEmpty()) {
			this.wantHairCut();
		}
	}

	public void wantHairCut() {

		mutexUp();
		barberDecrement(); 
		try {

			Customer c = (Customer) listOfCustomers.remove(0);
			System.out.println("Customer " + c.getID() + ": getting haircut");
			Thread.sleep(Barbershop.randomInt(1, 2) * 400);
			System.out.println("Barber: finished cutting customer " + c.getID()
					+ "'s hair");
			c.gotHaircut = true;

		} catch (InterruptedException e) {
		}

		cuttingIncrement();
	}

	public static int randomInt(int s, int e) {
		int start = s;
		int end = e;
		Random random = new Random();
		long range = (long) end - (long) start + 1;

		long fraction = (long) (range * random.nextDouble());
		int randomNumber = (int) (fraction + start);

		return randomNumber;
	}

	public synchronized void mutexDown() {
		while (mutexS == 0) {
			try {
				wait();
			} catch (InterruptedException ie) {
			}
		}
		mutexS--;
	}

	public synchronized void mutexUp() {
		mutexS++;
		notify();
	}

	public synchronized void customerDecrement() {
		while (c == 0) {
			try {
				wait();
			} catch (InterruptedException ie) {
			}
		}
		c--;
	}

	public synchronized void customerIncrement() {
		c++;
		notify();
	}

	public synchronized void barberDecrement() {
		while (b == 0) {
			try {
				wait();
			} catch (InterruptedException ie) {
			}
		}
		b--;
	}

	public synchronized void barberIncrement() {
		b++;
		notify();
	}

	public synchronized void cuttingDecrement() {
		while (cut == 0) {
			try {
				wait();
			} catch (InterruptedException ie) {
			}
		}
		cut--;
	}

	public synchronized void cuttingIncrement() {
		cut++;
		notify();
	}

}
