
class Barber extends Thread {

	private Barbershop barbershop;
	private boolean threadStop = false;
	private long start = 0;
	private long end = 0;

	public Barber(Barbershop barbershop) {
		this.barbershop = barbershop;
	}

	public boolean isRunning() {
		if (threadStop)
			return false;
		else
			return true;
	}

	@Override
	public void run() {
		start = System.currentTimeMillis();
		while (!threadStop) {
			barbershop.wantToCut();
			end = System.currentTimeMillis();

			if ((end - start) > 120000) {
				threadStop = true;
				RunBarberShop.waitTime();
				RunBarberShop.waitTime();
				RunBarberShop.waitTime();
				System.out
						.println("Barber: finished his shift. Barber has gone home");
			}
		}
	}
}
