
 
class Customer extends Thread {
 
     Barbershop barbershop;
     int id;
    
     boolean gotHaircut = false;
     boolean threadStop = false;
   public Customer(Barbershop barbershop, int id) {
      this.barbershop = barbershop;
      this.id = id;
    
   }

 
    public Barbershop getBarbershop() {
        return this.barbershop;
    }
 
    public void setBarbershop(Barbershop barbershop) {
        this.barbershop = barbershop;
    }
 
    public int getID(){
        return id;
    }
 
    public void setId(int id) {
        this.id = id;
    }
 
   public void run() {
 
       barbershop.customerArrived(this);
       while(!threadStop){
        try{
               sleep(100);
        }catch(InterruptedException e){}
       if(gotHaircut){
            System.out.println("Customer " + id + " left barbershop");
            threadStop = true;
            }
       }
   }
 
}
